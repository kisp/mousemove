package tools;

import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.WString;
import com.sun.jna.platform.win32.BaseTSD.LONG_PTR;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinNT.HANDLE;
import com.sun.jna.ptr.ByteByReference;
import com.sun.jna.ptr.IntByReference;

public class DummyUser32 implements User32 {

    // tester here :
    
    DWORD dummyDWORD;
    INPUT[] dummyArr;
    int dummyInt;

    public boolean verifySendInputCall(DWORD nInputs, INPUT[] pInputs, int cbSize) {
        return nInputs.equals(this.dummyDWORD) && pInputs.equals(this.dummyArr) && cbSize == this.dummyInt;
    }

    @Override
    public DWORD SendInput(DWORD nInputs, INPUT[] pInputs, int cbSize) {
        this.dummyDWORD = nInputs;
        this.dummyArr = pInputs;
        this.dummyInt = cbSize;
        return null;
    }
    
    // dummy below
    
    @Override
    public HDC GetDC(HWND hWnd) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int ReleaseDC(HWND hWnd, HDC hDC) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HWND FindWindow(String lpClassName, String lpWindowName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int GetClassName(HWND hWnd, char[] lpClassName, int nMaxCount) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean GetGUIThreadInfo(int idThread, GUITHREADINFO lpgui) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean GetWindowInfo(HWND hWnd, WINDOWINFO pwi) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean GetWindowRect(HWND hWnd, RECT rect) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int GetWindowText(HWND hWnd, char[] lpString, int nMaxCount) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int GetWindowTextLength(HWND hWnd) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int GetWindowModuleFileName(HWND hWnd, char[] lpszFileName, int cchFileNameMax) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int GetWindowThreadProcessId(HWND hWnd, IntByReference lpdwProcessId) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean EnumWindows(WNDENUMPROC lpEnumFunc, Pointer data) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean EnumChildWindows(HWND hWnd, WNDENUMPROC lpEnumFunc, Pointer data) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean EnumThreadWindows(int dwThreadId, WNDENUMPROC lpEnumFunc, Pointer data) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean FlashWindowEx(FLASHWINFO pfwi) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public HICON LoadIcon(HINSTANCE hInstance, String iconName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HANDLE LoadImage(HINSTANCE hinst, String name, int type, int xDesired, int yDesired, int load) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean DestroyIcon(HICON hicon) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int GetWindowLong(HWND hWnd, int nIndex) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int SetWindowLong(HWND hWnd, int nIndex, int dwNewLong) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public Pointer SetWindowLong(HWND hWnd, int nIndex, Pointer dwNewLong) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LONG_PTR GetWindowLongPtr(HWND hWnd, int nIndex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LONG_PTR SetWindowLongPtr(HWND hWnd, int nIndex, LONG_PTR dwNewLongPtr) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pointer SetWindowLongPtr(HWND hWnd, int nIndex, Pointer dwNewLongPtr) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean SetLayeredWindowAttributes(HWND hwnd, int crKey, byte bAlpha, int dwFlags) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean GetLayeredWindowAttributes(HWND hwnd, IntByReference pcrKey, ByteByReference pbAlpha, IntByReference pdwFlags) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean UpdateLayeredWindow(HWND hwnd, HDC hdcDst, POINT pptDst, SIZE psize, HDC hdcSrc, POINT pptSrc, int crKey,
            BLENDFUNCTION pblend, int dwFlags) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int SetWindowRgn(HWND hWnd, HRGN hRgn, boolean bRedraw) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean GetKeyboardState(byte[] lpKeyState) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public short GetAsyncKeyState(int vKey) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HHOOK SetWindowsHookEx(int idHook, HOOKPROC lpfn, HINSTANCE hMod, int dwThreadId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LRESULT CallNextHookEx(HHOOK hhk, int nCode, WPARAM wParam, LPARAM lParam) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LRESULT CallNextHookEx(HHOOK hhk, int nCode, WPARAM wParam, Pointer lParam) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean UnhookWindowsHookEx(HHOOK hhk) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int GetMessage(MSG lpMsg, HWND hWnd, int wMsgFilterMin, int wMsgFilterMax) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean PeekMessage(MSG lpMsg, HWND hWnd, int wMsgFilterMin, int wMsgFilterMax, int wRemoveMsg) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean TranslateMessage(MSG lpMsg) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public LRESULT DispatchMessage(MSG lpMsg) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void PostMessage(HWND hWnd, int msg, WPARAM wParam, LPARAM lParam) {
        // TODO Auto-generated method stub

    }

    @Override
    public void PostQuitMessage(int nExitCode) {
        // TODO Auto-generated method stub

    }

    @Override
    public int GetSystemMetrics(int nIndex) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public HWND SetParent(HWND hWndChild, HWND hWndNewParent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean IsWindowVisible(HWND hWnd) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean MoveWindow(HWND hWnd, int X, int Y, int nWidth, int nHeight, boolean bRepaint) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean SetWindowPos(HWND hWnd, HWND hWndInsertAfter, int X, int Y, int cx, int cy, int uFlags) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean AttachThreadInput(DWORD idAttach, DWORD idAttachTo, boolean fAttach) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean SetForegroundWindow(HWND hWnd) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public HWND GetForegroundWindow() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HWND SetFocus(HWND hWnd) {
        // TODO Auto-generated method stub
        return null;
    }
    

    @Override
    public DWORD WaitForInputIdle(HANDLE hProcess, DWORD dwMilliseconds) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean InvalidateRect(HWND hWnd, RECT lpRect, boolean bErase) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean RedrawWindow(HWND hWnd, RECT lprcUpdate, HRGN hrgnUpdate, DWORD flags) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public HWND GetWindow(HWND hWnd, DWORD uCmd) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean UpdateWindow(HWND hWnd) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean ShowWindow(HWND hWnd, int nCmdShow) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean CloseWindow(HWND hWnd) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean RegisterHotKey(HWND hWnd, int id, int fsModifiers, int vk) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean UnregisterHotKey(Pointer hWnd, int id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean GetLastInputInfo(LASTINPUTINFO plii) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ATOM RegisterClassEx(WNDCLASSEX lpwcx) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean UnregisterClass(WString lpClassName, HINSTANCE hInstance) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public HWND CreateWindowEx(int dwExStyle, WString lpClassName, String lpWindowName, int dwStyle, int x, int y, int nWidth, int nHeight,
            HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, LPVOID lpParam) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean DestroyWindow(HWND hWnd) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean GetClassInfoEx(HINSTANCE hinst, WString lpszClass, WNDCLASSEX lpwcx) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public LRESULT DefWindowProc(HWND hWnd, int Msg, WPARAM wParam, LPARAM lParam) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public HDEVNOTIFY RegisterDeviceNotification(HANDLE hRecipient, Structure notificationFilter, int Flags) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean UnregisterDeviceNotification(HDEVNOTIFY Handle) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int RegisterWindowMessage(String string) {
        // TODO Auto-generated method stub
        return 0;
    }
    
    

}
