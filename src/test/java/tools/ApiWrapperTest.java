package tools;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import static org.powermock.api.mockito.PowerMockito.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;


import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.DWORD;
import com.sun.jna.platform.win32.WinUser.INPUT;

public class ApiWrapperTest {
    
    ApiWrapper apiWrapper;
    
    @Mock
    User32 mockUser32;
    
    @Before
    public void setup() {
        apiWrapper = new ApiWrapper(mockUser32);
    }

    @Test
    public void shouldImplementNativeAPIWrapper() {
        assertEquals(mockUser32, apiWrapper.getUser32());
    }
    
    @Test
    public void shouldCallApi() {
        DummyUser32 user32 = new DummyUser32();
        
        DWORD dummyDWORD = new DWORD();
        INPUT dummyInput = new INPUT();
        INPUT [] dummyArr = new INPUT [] {dummyInput};
        
        int dummyInt = 0;
        
        ApiWrapper apiWrapper = new ApiWrapper(user32); 
        apiWrapper.moveMouse(dummyInput);
        
        user32.verifySendInputCall( dummyDWORD, dummyArr, dummyInt);
    } 
    
    @Test
    public void shouldCreateInputData() {
        assertNotNull( apiWrapper.createInput());
    }
    
    @Test
    public void shoudCreateDifferentInputDataForEachCall() {
        INPUT in1 = apiWrapper.createInput();
        INPUT in2 = apiWrapper.createInput();
        assertNotEquals(in1, in2);
    }
}
