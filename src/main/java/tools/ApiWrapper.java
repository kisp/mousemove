package tools;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef.*;
import com.sun.jna.platform.win32.WinUser.*;

public class ApiWrapper {

    public static final long MOUSEEVENTF_MOVE = 0x0001L;
    public static final long MOUSEEVENTF_LEFTDOWN = 0x0002L;
    public static final long MOUSEEVENTF_LEFTUP = 0x0004L;
    public static final long MOUSEEVENTF_VIRTUALDESK = 0x4000L;
    public static final long MOUSEEVENTF_ABSOLUTE = 0x8000L;

    final User32 lib;

    boolean chooseInput;

    protected ApiWrapper(User32 user32) {
        lib = user32;
    }

    protected User32 getUser32() {
        return lib;
    }
    
    protected void moveMouse(INPUT input) {
        DWORD numberOfInputs = new DWORD(1);
        int size = input.size();
        INPUT[] arr = { input };
        getUser32().SendInput(numberOfInputs, arr, size);
    }
    
    private int[] getNewCoordinates() {
        chooseInput = !chooseInput;
        if (chooseInput) {
            return new int[] {1,1};
        } else {
            return new int[] {10,10};
        }        
    }

    protected INPUT createInput() {
        INPUT input = new INPUT();

        input.type = new DWORD(INPUT.INPUT_MOUSE);
        input.input.setType("mi");
        int[] coords = getNewCoordinates();
        input.input.mi.dx = new LONG(coords[0] * 65536 / User32.INSTANCE.GetSystemMetrics(User32.SM_CXSCREEN));
        input.input.mi.dy = new LONG(coords[1] * 65536 / User32.INSTANCE.GetSystemMetrics(User32.SM_CYSCREEN));
        input.input.mi.mouseData = new DWORD(0);
        input.input.mi.dwFlags = new DWORD(MOUSEEVENTF_MOVE | MOUSEEVENTF_VIRTUALDESK | MOUSEEVENTF_ABSOLUTE);
        input.input.mi.time = new DWORD(0);

        return input;
    }
}
