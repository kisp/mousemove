package tools;

import com.sun.jna.platform.win32.Kernel32;
import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinUser.INPUT;

public class Main implements Runnable {

    private static final int TIME_DELAY_MILLIS = 10 * 1000;
    private static final int TIME_MAX_IDLE = 4 * 60 * 1000;
    private ApiWrapper api; 
    
    Main() {
        api = new ApiWrapper(User32.INSTANCE);
    }
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        Thread t = new Thread(new Main());
        t.start();
    }
    
    public static int getIdleTimeMillisWin32() {
        User32.LASTINPUTINFO lastInputInfo = new User32.LASTINPUTINFO();
        User32.INSTANCE.GetLastInputInfo(lastInputInfo);
        return Kernel32.INSTANCE.GetTickCount() - lastInputInfo.dwTime;
    }

    @Override
    public void run() {
        try {
            while(true) {
                if( getIdleTimeMillisWin32() > TIME_MAX_IDLE ) {
                    INPUT input = api.createInput();
                    api.moveMouse(input);
                }
                Thread.sleep(TIME_DELAY_MILLIS);
            }
        }
        catch (InterruptedException e) {

        }
    }

}
